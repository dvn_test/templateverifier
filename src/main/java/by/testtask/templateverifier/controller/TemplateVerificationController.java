package by.testtask.templateverifier.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import by.testtask.templateverifier.service.TemplateVerificationService;

@RestController
public class TemplateVerificationController {


  @Autowired
  private TemplateVerificationService templateVerficationService;

  @PostMapping("/validation/message")
  public String index(@RequestBody String message) {
    return templateVerficationService.verifyByTemplates(message);
  }
}
