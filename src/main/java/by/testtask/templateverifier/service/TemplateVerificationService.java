package by.testtask.templateverifier.service;

import com.opencsv.CSVReader;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import static java.lang.ClassLoader.getSystemResource;

@Service public class TemplateVerificationService {

  private static final Logger logger = LoggerFactory.getLogger(TemplateVerificationService.class);

  private static final String POSITIVE_ANSWER = "TRUE";
  private static final String NEGATIVE_ANSWER = "FALSE";

  private static final String SPECIAL_SYMBOLS = "[\\Q!No#%.,:;?/()+-\"―_'`&^{}[]<>|@$=~*\\Ea-zA-Zа-яА-Я0-9\\s]*";

  private static final String TEMPLATE_FILE_NAME = "templates.csv";
  private static final List<Pattern> TEMPLATES = new ArrayList<>();

  public String verifyByTemplates(String message) {
    if (TEMPLATES.stream().anyMatch(t -> t.matcher(message).matches())) {
      return POSITIVE_ANSWER;
    } else {
      return NEGATIVE_ANSWER;
    }
  }

  @PostConstruct private void initTemplates() throws IOException, URISyntaxException {
    List<String> templateStringList;
    URL fileWithTemplates = getSystemResource(TEMPLATE_FILE_NAME);
    if (fileWithTemplates == null) {
      throw new FileNotFoundException(TEMPLATE_FILE_NAME + " was not found in the classpath");
    }
    try (CSVReader csvReader = new CSVReader(Files.newBufferedReader(Paths.get(fileWithTemplates.toURI())))) {
      logger.info("Reading templates...");
      templateStringList = csvReader.readAll().stream().map(a -> a[0]).peek(logger::info).collect(Collectors.toList());
    }
    for (String templateString : templateStringList) {
      TEMPLATES.add(Pattern.compile(Arrays.stream(templateString.split(Pattern.quote("%w+"), -1)).map(s -> {
        if (!templateString.isEmpty()) {
          return Pattern.quote(s);
        } else {
          return s;
        }
      }).collect(Collectors.joining(SPECIAL_SYMBOLS))));
    }

  }
}
